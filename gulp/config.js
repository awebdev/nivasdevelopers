'use strict';

module.exports = {
  path: {
    dist: 'dist',
    js: ['js/**/*.js'],
    less: ['less/main.less'],
    templates: ['templates/**/*.mustache']
  },
  jscpdRules: {
    'min-lines': 10,
    'verbose': true
  }
};

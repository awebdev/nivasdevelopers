'use strict';

const gulp = require('gulp'),
  mustache = require('mustache'),
  fs = require('fs'),
  beautify = require('js-beautify').html,
  glob = require('glob'),
  path = require('path'),
  debug = require('debug')('nivasdevelopers:build:mustache');

gulp.task('templates', (cb) => {
  return glob('templates/partials/**/*.mustache', {}, function(err, partialFileNames) {
    if(err) {
      console.error('Failed to open/read path/dir');
      console.error(err);
      return;
    }

    let partials = {};
    partialFileNames.forEach(function(filename) {
      let basename = path.basename(filename, '.mustache');
      let partialName = filename.split(path.sep);
      partialName.shift(); // remove template prefix
      partialName.shift(); // remove partials prefix
      partialName.pop(); // remove filename from path
      partialName.push(basename); // add back the filename back
      partialName = partialName.join('/');

      debug('partials: ' + partialName + ' - ' + filename);
      partials[partialName] = fs.readFileSync(filename, 'utf8');
    });

    glob('templates/*.mustache', {}, function(err, templateNames) {
      templateNames.forEach(function(templateName) {
        let outFileName = path.basename(templateName, '.mustache');
        let dataFilename = 'data/' + outFileName + '.json';
        let htmlPath = 'dist/' + outFileName + '.html';

        fs.lstat(dataFilename, function(err, stats) {
          let data = {};

          if(err) {
            debug('Failed to open ' + dataFilename + ' skipping...');
          } else if(stats.isFile()) {
            debug('data:' + dataFilename);
            try {
              data = fs.readFileSync(path.resolve(dataFilename), 'utf8');
              data = JSON.parse(data);
            } catch (e) {
              data = {};
              console.error('Failed to JSON.parse ' + dataFilename);
            }

            for (var partialName in partials) {
              debug('partials: compiling: ' + partialName);
              partials[partialName] = mustache.render(partials[partialName], data);
            }
          }

          let template = fs.readFileSync(templateName, 'utf8');
          let html = mustache.render(template, data, partials);

          debug('out: ' + htmlPath);
          fs.writeFileSync(htmlPath, beautify(html, {
            'indent_size': 2
          }));
        });

        cb();
      });
    });
  });
});

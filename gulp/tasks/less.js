'use strict';

const gulp = require('gulp'),
  path = require('path'),
  rename = require("gulp-rename"),
  config = require(path.resolve('gulp/config')),
  less = require('gulp-less'),
  cssmin = require('gulp-cssmin');

gulp.task('less', () => {
  return gulp.src(config.path.less)
    .pipe(less().on('error', function(err) {
      console.log(err);
    }))
    .pipe(cssmin().on('error', function(err) {
      console.log(err);
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/css'));
});

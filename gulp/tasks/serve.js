'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  runSequence = require('run-sequence');

gulp.task('serve', (cb) => {
  runSequence(
    'build',
    'watch',
    cb
  );
});

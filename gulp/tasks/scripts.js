'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  runSequence = require('run-sequence'),
  uglify = require('gulp-uglify');

gulp.task('scripts', () => {
  return gulp.src(config.path.js)
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

'use strict';

const gulp = require('gulp'),
  runSequence = require('run-sequence');

gulp.task('build', (cb) => {
  runSequence(
    'clean',
    'lint',
    ['less', 'images', 'templates', 'copyto', 'scripts'],
    cb
  );
});

'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  del = require('del');

gulp.task('clean', () => {
  return del([config.path.dist]);
});

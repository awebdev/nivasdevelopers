'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  jscs = require('gulp-jscs'),
  debug = require('gulp-debug'),
  runSequence = require('run-sequence');

gulp.task('lint', () => {
  return gulp.src(config.path.js, {base: "./"})
    .pipe(debug({title: 'lint:client: '}))
    .pipe(jscs({configPath: '.jscsrc'}))
    .pipe(jscs({fix: true}))
    .pipe(jscs.reporter(''))
    .pipe(jscs.reporter('failImmediately'))
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'));
});

'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  imagemin = require('gulp-imagemin'),
  favicons = require('gulp-favicons');

gulp.task('images', ['imagemin', 'favicons']);

gulp.task('imagemin', () => {
  return gulp.src('images/**/*')
    .pipe(imagemin({progressive: true, verbose: true}))
    .pipe(gulp.dest('dist/images'))
});

gulp.task('favicons', () => {
  return gulp.src('images/nivas-logo.png')
    .pipe(favicons({
      appName: 'Nivas Developers',
      appDescription: '',
      path: 'images/',
      url: 'http://nivasdevelopers.com/',
      display: 'standalone',
      orientation: 'portrait',
      version: 1.0,
      logging: false,
      online: false,
      replace: false,
      icons: {
        android: true,              // Create Android homescreen icon. `boolean`
        appleIcon: true,            // Create Apple touch icons. `boolean`
        appleStartup: true,         // Create Apple startup images. `boolean`
        coast: true,                // Create Opera Coast icon. `boolean`
        favicons: true,             // Create regular favicons. `boolean`
        firefox: true,              // Create Firefox OS icons. `boolean`
        windows: true,              // Create Windows 8 tile icons. `boolean`
        yandex: true                // Create Yandex browser icon. `boolean`
      }
    })).pipe(gulp.dest(config.path.dist));
});

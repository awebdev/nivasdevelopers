'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config')),
  gls = require('gulp-live-server'),
  runSequence = require('run-sequence');

gulp.task('watch', () => {
  // less
  gulp.watch(['less/**/*.less'], ['less']);

  // images
  gulp.watch(['images/**/*'], ['images']);

  // templates
  gulp.watch(config.path.templates, ['templates']);
  gulp.watch(['data/**/*'], ['templates']);

  // scripts
  gulp.watch(config.path.js, ['scripts']);

  // copyto
  gulp.watch(['config/**/*', 'assets/**/*'], ['copyto']);

  var appServer = gls.static(config.path.dist, 8080);
  appServer.start();
});

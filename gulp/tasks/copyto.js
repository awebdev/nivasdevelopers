'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config'));

gulp.task('copyto', ['copyto:config', 'copyto:assets']);

gulp.task('copyto:config', () => {
  return gulp.src('config/**/*')
    .pipe(gulp.dest(config.path.dist));
});

gulp.task('copyto:assets', () => {
  return gulp.src('assets/**/*')
    .pipe(gulp.dest('dist/assets'));
});

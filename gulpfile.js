'use strict';

const gulp = require('gulp'),
  path = require('path'),
  config = require(path.resolve('gulp/config'));

require('require-dir')(path.resolve('gulp/tasks'));

gulp.task('default', ['serve']);

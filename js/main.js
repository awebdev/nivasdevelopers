'use strict';

window.GoogleAnalyticsObject = '__ga__';
window.__ga__ = {
  q: [['create', 'UA-81192606-1', 'auto']],
  l: Date.now()
};

require.config({
  paths: {
    'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min',
    'ga': '//www.google-analytics.com/analytics',
    'jquery': '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min'
  },
  shim: {
    'ga': {
      exports: '__ga__'
    },
    'bootstrap' : ['jquery']
  }
});

require(['ga', 'jquery', 'bootstrap'], function(ga, $) {
  ga('send', 'pageview');

  $('.carousel').carousel({
    interval: 5000
  });

  // Feature detects Navigation Timing API support.
  if (window.performance) {
    // Gets the number of milliseconds since page load
    // (and rounds the result since the value must be an integer).
    var timeSincePageLoad = Math.round(window.performance.now());

    // Sends the timing hit to Google Analytics.
    ga('send', 'event', 'timing', 'load', 'JS load time', timeSincePageLoad);
  }

  $('.js-show-brochure').click(function(evt) {
    ga('send', 'event', 'click', 'show-brochure', 'brochure');

    var brochureUrl = "assets/Golden-Palms-Brochure-0816.pdf";
    var win = window.open(brochureUrl, '_blank');
    win.focus();
  });
});

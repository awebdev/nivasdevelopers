# Powers http://nivasdevelopers.com website

## Cloning
```
$ git clone https://github.com/awebdev/nivasdevelopers.git
```
## Get started
```
// To start the project at http://127.0.0.1:8080
$ npm run start

// To start watching for changes and build files
$ npm run watch
```

## Test
```
$ npm run test
```

## Deploy
```
$ git subtree push --prefix dist origin gh-pages
```

## Thanks to
* BlackrockDigital (https://github.com/BlackrockDigital/startbootstrap-business-casual)
